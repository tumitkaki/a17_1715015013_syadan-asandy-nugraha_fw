<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;

class orderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(5);
        return view('orders.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('orders.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'inv_no' => 'required|min:5|max:20',
            'tot_har' => 'required|numeric',
            'status'=> 'required'
        ]);
        $new_order = new Order();
        $new_order->user_id = $request->get('user_id');
        $new_order->inv_no = $request->get('inv_no');
        $new_order->tot_har = $request->get('tot_har');
        $new_order->status = $request->get('status');
        $new_order->save();

        return redirect()->route('orders.index')->with('status','Order Succesfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('users')->find($id);
        return view('orders.show',['order'=>$order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::with('users')->find($id);
        $users = User::all();
        return view('orders.edit',['order'=>$order], compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
          'inv_no' => 'required|min:5|max:30',
          'user_id' => 'required',
          'tot_har' => 'required|numeric',
          'status'=> 'required'
      ]);
      $update_order = Order::findOrFail($id);
      $update_order->inv_no = $request->get('inv_no');
      $update_order->user_id = $request->get('user_id');
      $update_order->tot_har = $request->get('tot_har');
      $update_order->status = $request->get('status');
      $update_order->save();
      return redirect()->route('orders.index')->with('status','Order Succesfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $order = Order::findOrFail($id);
         $order->delete();
         return redirect()->route('orders.index')->with('status', 'Order Successfully deleted');
     }

     public function search(Request $request)
     {
         $orders = Order::when($request->found, function ($query) use ($request) {
           $query->where('inv_no', 'like', "%{$request->found}%")->
           where('status', 'like', "%{$request->statuss}%");
         })->paginate(5);
         return view('orders.index', compact('orders'));
     }
}
