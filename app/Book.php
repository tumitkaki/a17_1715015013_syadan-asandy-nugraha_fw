<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'description', 'author', 'publisher', 'cover', 'price', 'stock'
    ];

    public function category() {
        return $this->belongsToMany(Category::class);
    }
}
