<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
          [
              'cat_name' => 'Drama',
              'desc' => 'Penuh dengan pembualan yang dapat membawa pada kesialan'
          ],
          [
              'cat_name' => 'Romance',
              'desc' => 'Kisah romantis, asmara, percintaan'
          ],
          [
              'cat_name' => 'Adventure',
              'desc' => 'Petualangan yang memberi pengetahuan dan pengalaman'
          ],
          [
              'cat_name' => 'Sport',
              'desc' => 'Olahraga, Olahtubuh, Olahbadan, Olahjasmani, Olahrohani, Olahfisik'
          ],
          [
              'cat_name' => 'Family',
              'desc' => 'Keluarga, seru-seruan, kehangatan'
          ],
          [
              'cat_name' => 'Fantasy',
              'desc' => 'Cerita penuh dengan dusta dan kebohongan agar orang berkhayal'
          ],
          [
              'cat_name' => 'Thriller',
              'desc' => 'Menegangkan, mencekam, ketakutan yg hakiki'
          ],
          [
              'cat_name' => 'Comedy',
              'desc' => 'seru-seruan, lucu, humor, ngakak, wkwkwk, lol'
          ],
          [
              'cat_name' => 'Technology',
              'desc' => 'Penuh dengan hal modern dan canggih, perkembangan IT terkini'
          ],
          [
              'cat_name' => 'Programming',
              'desc' => 'Segala bahasa pemrograman'
          ],
          [
              'cat_name' => 'Science',
              'desc' => 'Hal magis yang dapat dinalar dan dicerna serta diserap kemudian diterima oleh manusia'
          ],
          [
              'cat_name' => 'Language',
              'desc' => 'Bahasa seluruh dunia, kamus 1 Milyar Triliun Juta'
          ],
          [
              'cat_name' => 'Religion',
              'desc' => 'Agama seluruh dunia ada disini, kitab-kitab, dan cerita'
          ],
          [
              'cat_name' => 'Constitutional Law',
              'desc' => 'Semua hukum dan ketatanegaraan ada disini, hukum alam, hukum perdata, hingga hukum fiqh'
          ],
          [
              'cat_name' => 'Economy n Business',
              'desc' => 'Buku yg membahas mengenai ekonomi dunia yg memiliki sumber daya terbatas'
          ],
          [
              'cat_name' => 'Motivation',
              'desc' => 'Bacotan penuh makna dari para orang sukses dunia yang menjadi motivator'
          ],
      ]);
    }
}
