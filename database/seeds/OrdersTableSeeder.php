<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'inv_no' => 23654568,
                'tot_har' => 150000,
                'status' => 'PROCESS',
                'user_id' => 1
            ],
            [
                'inv_no' => 21457451,
                'tot_har' => 100000,
                'status' => 'FINISH',
                'user_id' => 2
            ],
            [
                'inv_no' => 58458745,
                'tot_har' => 135000,
                'status' => 'SUBMIT',
                'user_id' => 3
            ],
            [
                'inv_no' => 65985487,
                'tot_har' => 230000,
                'status' => 'PROCESS',
                'user_id' => 4
            ],
            [
                'inv_no' => 87546895,
                'tot_har' => 420000,
                'status' => 'CANCEL',
                'user_id' => 5
            ],
            [
                'inv_no' => 23454568,
                'tot_har' => 120000,
                'status' => 'PROCESS',
                'user_id' => 6
            ],
            [
                'inv_no' => 189273871,
                'tot_har' => 150000,
                'status' => 'FINISH',
                'user_id' => 7
            ],
            [
                'inv_no' => 989082348,
                'tot_har' => 111000,
                'status' => 'SUBMIT',
                'user_id' => 8
            ],
            [
                'inv_no' => 993889472,
                'tot_har' => 239000,
                'status' => 'PROCESS',
                'user_id' => 9
            ],
            [
                'inv_no' => 387281995,
                'tot_har' => 500000,
                'status' => 'CANCEL',
                'user_id' => 10
            ],
            [
                'inv_no' => 128738912,
                'tot_har' => 200000,
                'status' => 'PROCESS',
                'user_id' => 11
            ],
            [
                'inv_no' => 120098334,
                'tot_har' => 190000,
                'status' => 'FINISH',
                'user_id' => 12
            ],
            [
                'inv_no' => 812736873,
                'tot_har' => 80000,
                'status' => 'SUBMIT',
                'user_id' => 13
            ],
            [
                'inv_no' => 1726377737,
                'tot_har' => 400000,
                'status' => 'PROCESS',
                'user_id' => 14
            ],
            [
                'inv_no' => 9973612673,
                'tot_har' => 900000,
                'status' => 'CANCEL',
                'user_id' => 15
            ]
        ]);
    }
}
