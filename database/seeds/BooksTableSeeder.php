<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'title' => 'Hujan',
                'author' => 'Tere Liye',
                'publisher' => 'Elex Media',
                'price' => '75000',
                'stock' => '10',
                'description' => 'Berawal dari pertemuan Lail dengan Elijah di sebuah ruangan terapi.
                Lail menemani Elijah hanya untuk satu tujuan: ingin menghapus ingatannya tentang hujan.',
                'cover' => ''
            ],
            [
                'title' => 'Sang Ahli Kimia',
                'author' => 'Stephenie Meyer',
                'publisher' => 'mizan',
                'price' => '60000',
                'stock' => '13',
                'description' => 'Sebagai mantan agen, ia menyimpan rahasia tergelap agensi yang membuatnya menjadi incaran pemerintah Amerika.
                Mereka ingin ia mati. Ia hidup dalam pelarian selama hampir tiga tahun.',
                'cover' => ''
            ],
            [
                'title' => 'Seventeen Once Again',
                'author' => 'Handi Namire',
                'publisher' => 'Erlangga',
                'price' => '40000',
                'stock' => '40',
                'description' => 'Raka akhirnya membalas perasaan Briana. Ya, Raka, ketua OSIS yang digandrungi banyak siswi di sekolah. Raka
                yang juga pacar Tara, sahabat baiknya.' ,
                'cover' => ''
            ],
            [
                'title' => 'Bad Boys 2',
                'author' => 'Nathalia Theodora',
                'publisher' => 'Elex Media',
                'price' => '50000',
                'stock' => '30',
                'description' => 'Memang lagi jaman ya, anak SMA jaman sekarang main geng-geng-an? Itu-lah yang sempat terlintas di benak Sophie
                Wyna, salah satu murid SMA Emerald.',
                'cover' => ''
            ],
            [
                'title' =>'LET ME BE WITH YOU',
                'author' => 'Ria N. Badaria',
                'publisher' => 'Shonen Jump',
                'price' => '80000',
                'stock' => '15',
                'description' => 'Tidak tahan karena terus didesak menikah oleh keluarganya. Kinanti akhirnya menerima ide gila Rivan Arya,
                sahabat kakaknya yang telah ia kenal sejak SMA. ',
                'cover' => ''
            ],
            [
                'title'=>'PHP For Nolep',
                'author'=>'Chocky Gerung',
                'publisher'=>'KOM MAS',
                'price'=>'69000',
                'stock'=>'10',
                'description'=>'',
                'cover'=>'cover/1.jpg'
            ],
            [
                'title'=>'Ternyata Ibuku Adalah Ayahku',
                'author'=>'Nopan And Friends',
                'publisher'=>'Mahardika',
                'price'=>'35000',
                'stock'=>'10',
                'description'=>'',
                'cover'=>'cover/3.jpg'
            ],
            [
                'title'=>'Story Of Audrey',
                'author'=>'Eshfand El Farsiyy',
                'publisher'=>'Iranian Publish Her',
                'price'=>'35000',
                'stock'=>'10',
                'description'=>'',
                'cover'=>'cover/8.jpg'
            ],
            [
                'title'=>'Zodiac For Life',
                'author'=>'Lord Umam',
                'publisher'=>'Hellyah',
                'price'=>'12000',
                'stock'=>'10',
                'description'=>'',
                'cover'=>'cover/12.jpg'
            ],
            [
                'title'=>'Ekonomi Masyarakat Terpencil',
                'author'=>'Jamrud Khat',
                'publisher'=>'Informa Pub',
                'price'=>'30000',
                'stock'=>'10',
                'description'=>'',
                'cover'=>'cover/15.jpg'
            ],
            [
                'title'=>'Menguasai Efek Khusus dengan Photoshop',
                'author'=>'Jubilee Enterprise',
                'publisher'=>'Yuda Media',
                'price'=>'45000',
                'stock'=>'15',
                'description'=>'Melakukan edit foto pada photoshop',
                'cover'=>'cover/book2.jpg'
            ],
            [
                'title'=>'Bad Romance',
                'author'=>'Milliandia',
                'publisher'=>'Surya',
                'price'=>'150000',
                'stock'=>'15',
                'description'=>'-',
                'cover'=>'cover/book6jpg'
            ],
            [
                'title'=>'Tuilet',
                'author'=>'Oben Cedric',
                'publisher'=>'PT. Grasindo',
                'price'=>'100000',
                'stock'=>'15',
                'description'=>'Novel bertema humor',
                'cover'=>'cover/book8.jpg'
            ],
            [
                'title'=>'Sabar tanpa batas',
                'author'=>'M. Nurroziqri',
                'publisher'=>'PT. Grasindo',
                'price'=>'100000',
                'stock'=>'15',
                'description'=>'sabar itu tidak ada batasnya',
                'cover'=>'cover/book14.jpg'
            ],
            [
                'title'=>'Kitab komik filsuf muslim',
                'author'=>'Ibod',
                'publisher'=>'PT. Grasindo',
                'price'=>'100000',
                'stock'=>'15',
                'description'=>'-',
                'cover'=>'cover/book13.jpg'
            ]
        ]);
    }
}
