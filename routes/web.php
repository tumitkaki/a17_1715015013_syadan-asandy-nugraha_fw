<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/templates/home');
});

Route::resource('/users','userController');
Route::resource('/categories','categoryController');
Route::resource('/books','bookController');
Route::resource('/orders','orderController');

Route::get('/users/search/email', 'userController@search')->name('users.search');
Route::get('/books/search/title', 'bookController@search')->name('books.search');
Route::get('/categories/search/category', 'categoryController@search')->name('categories.search');
Route::get('/orders/search/pesanan', 'orderController@search')->name('orders.search');
